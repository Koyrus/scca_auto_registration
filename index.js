var express  = require('express');
var app = express();
var cors = require('cors')
var bodyParser = require('body-parser');
var request = require("request");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Credentials', true);
  next();
});


function execute(command) {
  const exec = require('child_process').exec

  exec(command, (err, stdout, stderr) => {
    process.stdout.write(stdout)
  })
}

setInterval(function(){
  request({
    uri: "https://programari.registru.md/wp-admin/admin-ajax.php",
    method: "POST",
    timeout: 0,
    followRedirect: true,
    maxRedirects: 10,
    form: {
      action: 'GetProviderAvailableDatesByService',
      ProviderId: 36,
      ServiceId: 338
    }
  }, function(error, response, body) {


    for(let i = 0;i <= 31 ; i++) {
      //@TODO Uncomment it to enable search for july and june
      // if (body.includes(`2019-07-${i}`)) {
      //
      //   let result = JSON.parse(body).filter(item => {
      //     return item.match(`2019-07`)
      //   })
      //   console.log('Allow 07 ----------------------------------' , result)
      //   execute(`notify-send "July ${result}"`)
      // }
      // if (body.includes(`2019-06-${i}`)) {
      //   let result = JSON.parse(body).filter(item => {
      //     return item.match(`2019-06`)
      //   })
      //   console.log('Allow 06 +++++++++++++++++++++++++++++++++' , result)
      //   execute(`notify-send "June ${result}"`)
      //
      // }
      if (body.includes(`2019-05-${i}`)) {
        //@TODO Need to find better solution instead of this one.
        let result = JSON.parse(body).filter(item => {
          return item.match(`2019-05`)
        });
        console.log('Allow 05 +++++++++++++++++++++++++++++++++' , result)
        execute(`notify-send "May ${result}"`)

      }

      if (body.includes(`2019-04-${i}`)) {
        let result = JSON.parse(body).filter(item => {
          return item.match(`2019-04`)
        });
        console.log('Allow 04 +++++++++++++++++++++++++++++++++' , result)
        execute(`notify-send "April!! ${result}"`)

      //@TODO Need to make execute for Windows. Now it works only for MAC/Linux systems.
      }

    }
  });
},20000);



app.listen(3000 , function () {

  console.log('Api working on 3000')
});
