var express  = require('express');
var app = express();
var cors = require('cors')
var bodyParser = require('body-parser');
var request = require("request");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Credentials', true);
  next();
});

function execute(command) {
  const exec = require('child_process').exec

  exec(command, (err, stdout, stderr) => {
    process.stdout.write(stdout)
  })
}


setInterval(function(){
  request({
    uri: "https://programari.registru.md/wp-admin/admin-ajax.php",
    method: "POST",
    timeout: 0,
    followRedirect: true,
    maxRedirects: 10,
    form: {
      action: 'GetProviderAvailableDatesByService',
      ProviderId: 36,
      ServiceId: 417
    }
  }, function(error, response, body) {
    console.log(body)
    if(body.length > 2){
      execute(`notify-send "${body}"`)
    }
  });
},15000)



app.listen(3001 , function () {

  console.log('Api working on 3000')
});
